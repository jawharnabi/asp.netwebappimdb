﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationIMDB.Models
{
    public class Movies
    {
        public int MovieId { get; set; }
        public string MovieName { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Director { get; set; }
        public string Country { get; set; }

    }
}