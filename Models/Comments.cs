﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplicationIMDB.Models
{
    public class Comments
    {
        public int CommentId { get; set; }

        [ForeignKey("Movies")]
        public int MovieCommented { get; set; }

        [ForeignKey("Users")]
        public int CommentAuthor { get; set; }
        [Range(1, 10)]

        public int Comment { get; set; }
        public DateTime Date { get; set; }
    }
}