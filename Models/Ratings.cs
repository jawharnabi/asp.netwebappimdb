﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplicationIMDB.Models
{
    public class Ratings
    {
        public int RatingId { get; set; }

        [ForeignKey("Movies")]
        public int MovieRated { get; set; }

        [ForeignKey("Users")]
        public int UserRating { get; set; }
        [Range(1, 10)]

        public int Rating { get; set; }
    }
}